<?php

namespace App;

// On use tous les repositories
use App\Repositories\UserRepository;
use App\Repositories\PropertyRepository;

// Singleton du singleton Repository qui est l'intermédiaire qui gère tous les repositories
// et par lequel on doit passer pour interagir avec les diffépropertys repositories
class RepositoryManager
{
    private static ?RepositoryManager $instance = null;

    // On déclare les variables d'accès à chaque repository ainsi que leur getter
    private UserRepository $userRepository;
    public function getUserRepository(): UserRepository { return $this->userRepository; }
    private PropertyRepository $propertyRepository;
    public function getPropertyRepository(): PropertyRepository { return $this->propertyRepository; }

    public static function getRepositoryManager(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
        // Lors de la création du RepositoryManager, on instancie toutes les repositories
        // que l'on a initialisé au début de la classe
        $this->userRepository = new UserRepository();
        $this->propertyRepository = new PropertyRepository();
    }

    private function __clone(){}
    private function __wakeup(){}
}
