<?php

namespace App;

class Session
{
    // Authentification
    public const SESSION_USER = 'SESSION_USER';
    
    // Formulaires
    public const SESSION_FORM_STATUS = 'SESSION_FORM_STATUS';

    // Sécurité
    public const SESSION_ANTI_CSRF_TOKEN = 'SESSION_ANTI_CSRF_TOKEN';

    // Fonction permettant de récupérer le nom de la session
    public static function get( string $sessionName )
    {
        if( isset( $_SESSION[ $sessionName ] ) ) {
            return $_SESSION[ $sessionName ];
        }

        return null;
    }

    // Fonction permettant de set le nom de la session et de lui attribuer une
    // variable, cette variable est utilisée pour stocker le token
    public static function set( string $sessionName, $value ): void
    {
        $_SESSION[ $sessionName ] = $value;
    }
}