<?php

namespace App\Controllers;

use Laminas\Diactoros\ServerRequest;

use App\Forms\AntiCsrf;
use App\RepositoryManager;
use App\Session;
use App\Views\View;

class RegController extends Controller
{
    // Page s'enregistrer (GET)
    public function registerView(): void
    {
        $view = new View('register');
        $view->templateOnly = true;

        // On passe les variables à la view dans le tableau $data, la view peut directement accéder aux variables dans ce tableau
        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
            'form_status' => Session::get(Session::SESSION_FORM_STATUS)
        ];

        Session::set(Session::SESSION_FORM_STATUS, null);

        $view->render($data);
    }

    // Enregistrement du nouveau compte dans la bdd (POST)
    public function register(ServerRequest $request): void
    {
        $postData = $request->getParsedBody();

        $this->csrfGuard($postData['csrf']);

        //Si il y a eu une erreur, alors on renvoie sur la même page, si tout s'est bien passé, on renvoie sur la page d'accueil
        if(!RepositoryManager::getRepositoryManager()->getUserRepository()->register($postData['firstname'], $postData['lastname'], $postData['email'], $postData['password'], $postData['checkbox_pro'])) {
            header('Location: /register');
        } else {
            header('Location: /login');
        }
    }
}
