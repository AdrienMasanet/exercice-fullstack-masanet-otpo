<?php

namespace App\Controllers;

use App\Forms\AntiCsrf;
use Laminas\Diactoros\ServerRequest;

use App\Forms\FormStatus;
use App\RepositoryManager;
use App\Session;
use App\Views\View;

class PageController extends Controller
{
    public function index()
    {
        $view = new View("accueil");

        // On passe les variables à la view dans le tableau $data, la view peut directement accéder aux variables dans ce tableau
        $data = [];

        $view->render($data);
    }

    // Fonction qui retourne un objet json contenant tous les posts approuvés par les admins (GET)
    public function returnMapMarkers()
    {
        // On récupère notre liste de posts approuvés dans la bdd
        $mapMarkers = RepositoryManager::getRepositoryManager()->getPropertyRepository()->findAllValidatedProperties();

        return json_encode($mapMarkers);
    }
}
