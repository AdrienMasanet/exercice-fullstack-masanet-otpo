<?php

namespace App\Controllers;

use Laminas\Diactoros\ServerRequest;

use App\Forms\AntiCsrf;
use App\Forms\FormStatus;
use App\RepositoryManager;
use App\Session;
use App\Views\View;

class AuthController extends Controller
{
    // Page de login (GET)
    public function loginView(): void
    {
        $view = new View('login');
        $view->templateOnly = true;

        // On passe les variables à la view dans le tableau $data, la view peut directement accéder aux variables dans ce tableau
        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
            'form_status' => Session::get(Session::SESSION_FORM_STATUS)
        ];

        Session::set(Session::SESSION_FORM_STATUS, null);

        $view->render($data);
    }

    // Authentification (POST)
    public function login(ServerRequest $request): void
    {
        $postData = $request->getParsedBody();

        $this->csrfGuard($postData["csrf"]);

        $user = RepositoryManager::getRepositoryManager()->getUserRepository()->login($postData['email'], $postData['password']);

        if (!is_null($user)) {
            Session::set(Session::SESSION_USER, $user);

            $status = new FormStatus();
            $status->success = true;
            $status->message = "Connexion réussie !";

            header('Location: /');
            return;
        }

        $status = new FormStatus();
        $status->success = false;
        $status->message = "Email/mot de passe invalide !";

        Session::set(Session::SESSION_FORM_STATUS, $status);

        header('Location: /login');
    }

    // Logout (GET)
    public function logout(): void
    {
        unset($_SESSION);
        session_destroy();

        header('Location: /');
    }
}
