<?php

namespace App\Controllers;

use App\Forms\AntiCsrf;
use Laminas\Diactoros\ServerRequest;

use App\Forms\FormStatus;
use App\RepositoryManager;
use App\Session;
use App\Views\View;

class ProController extends Controller
{
    // Affichage de la page permettant de rajouter des biens (GET)
    public function addPropertyView(): void
    {
        $view = new View("addpropertypanel");

        // On passe les variables à la view dans le tableau $data, la view peut directement accéder aux variables dans ce tableau
        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
        ];

        $view->render($data);
    }

    // Enregistrement du nouveau bien dans la bdd (POST)
    public function addProperty(ServerRequest $request): void
    {
        $postData = $request->getParsedBody();

        $this->csrfGuard($postData['csrf']);

        // On récupère l'ID et el mail de l'user qui créé le bien pour les stocker dans les colonnes prévues à cet effet
        // dans la row du bien dans la bdd
        $user = Session::get(Session::SESSION_USER);
        $attached_user_id = $user->id;
        $attached_user_mail = $user->email;

        // Le bien n'est pas approuvé par défaut, mais si celui qui a déclenché cette fonction est un admin
        // alors le bien est automatiquement approuvé
        $property_approved = 0;
        if ($user->is_admin) {
            $property_approved = 1;
        }

        // Si il y a eu une erreur, alors on renvoie sur la même page, si tout s'est bien passé, on renvoie sur la page d'accueil
        RepositoryManager::getRepositoryManager()->getPropertyRepository()->addProperty($attached_user_id, $attached_user_mail, $postData['name'], $postData['checkbox_property_type'], $postData['adress'], $postData['rooms'], $postData['stuff'], $postData['description'], $postData['checkbox_show_mail'], $property_approved);
        header('Location: /');
    }
}
