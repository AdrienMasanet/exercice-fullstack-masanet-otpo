<?php

namespace App\Controllers;

use App\Forms\AntiCsrf;
use Laminas\Diactoros\ServerRequest;

use App\Forms\FormStatus;
use App\RepositoryManager;
use App\Session;
use App\Views\View;

class AdminController extends Controller
{
    // Page du panneau d'administration (GET)
    public function admin(): void
    {
        $view = new View("adminpanel");

        // On passe les variables à la view dans le tableau $data, la view peut directement accéder aux variables dans ce tableau
        $data = [];

        $view->render($data);
    }

    // Page de gestion des membres inscrits sur le site (GET)
    public function manageUsers(): void
    {
        $view = new View("manageuserspanel");

        $users = RepositoryManager::getRepositoryManager()->getUserRepository()->findAllUsers();

        // On passe les variables à la view dans le tableau $data, la view peut directement accéder aux variables dans ce tableau
        $data = [
            "users" => $users
        ];

        $view->render($data);
    }

    // Page de gestion des membres pro dans le panneau d'administration (GET)
    public function addPro(): void
    {
        $view = new View("addpropanel");

        // On créé deux arrays : un contenant tous les pro déjà validés et ceux dont la demande est en attente
        $usersWaitingForApproval = RepositoryManager::getRepositoryManager()->getUserRepository()->findAllUsersWaitingForApproval();
        $proUsers = RepositoryManager::getRepositoryManager()->getUserRepository()->findAllProUsers();

        // On merge les deux arrays : les utilisateurs en attente d'être acceptés apparaîtront toujours en premier
        $users = array_merge($usersWaitingForApproval, $proUsers);

        // On passe les variables à la view dans le tableau $data, la view peut directement accéder aux variables dans ce tableau
        $data = [
            "users" => $users
        ];

        $view->render($data);
    }

    // Appel à la fonction de UserRepository permettant de toogle l'état pro d'un membre (POST)
    public function togglePro(ServerRequest $request): void
    {
        $postData = $request->getParsedBody();

        // On toggle le statut pro de l'utilisateur correspondant à l'id $postData["id"]
        RepositoryManager::getRepositoryManager()->getUserRepository()->toggleIsPro($postData["id"]);

        // On désapprouve tous les posts qui ont déjà été validés si leur utilisateur a perdu son statut de pro
        RepositoryManager::getRepositoryManager()->getPropertyRepository()->disapprovePropertiesIfOwnerIsNotPro();

        header('Location: /admin/addpro');
    }

    // Action de supprimer un utilisateur (POST)
    public function deleteUser(ServerRequest $request): void
    {
        $postData = $request->getParsedBody();

        RepositoryManager::getRepositoryManager()->getUserRepository()->deleteById($postData["id"]);

        header('Location: /admin/' . $postData["redirection"]);
    }

    // Page de validation des posts des pro dans le panneau d'administration (GET)
    public function validatePropertiesView(): void
    {
        $view = new View("validatepropertiespanel");

        $properties = RepositoryManager::getRepositoryManager()->getPropertyRepository()->findAllPropertiesWaitingForApproval();

        // On passe les variables à la view dans le tableau $data, la view peut directement accéder aux variables dans ce tableau
        $data = [
            'csrf_token' => AntiCsrf::generateToken(),
            "properties" => $properties
        ];

        $view->render($data);
    }

    // Action de valider un bien en demande de validation (POST)
    public function validateProperty(ServerRequest $request): void
    {
        $postData = $request->getParsedBody();

        RepositoryManager::getRepositoryManager()->getPropertyRepository()->changeApprovedStatus($postData["id"], 1);

        header('Location: /admin/validateproperty');
    }

    // Action de supprimer la demande de validation d'un bien (POST)
    public function removeProperty(ServerRequest $request): void
    {
        $postData = $request->getParsedBody();

        RepositoryManager::getRepositoryManager()->getPropertyRepository()->deleteById($postData["id"]);

        header('Location: /admin/validateproperty');
    }
}
