<?php

namespace App\Controllers;

use MiladRahimi\PhpRouter\Router;
use Laminas\Diactoros\Response\HtmlResponse;

use App\App;
use App\Forms\AntiCsrf;
use App\Session;

abstract class Controller
{
    public ?Router $router = null;

    public function __construct()
    {
        $this->router = App::getInstance()->getRouter();
    }

    // Fonction permettant aux Controllers de récupérer les infos de la
    // session actuelle : si l'utilisateur est connecté, si il est admin, etc
    protected function getCommonData(): array
    {
        $user = Session::get(Session::SESSION_USER);
        $isAuth = !is_null($user);
        $is_admin = $isAuth && $user->is_admin;

        return [
            "isAuth" => $isAuth,
            "is_admin" => $is_admin,
            "user" => $user
        ];
    }

    public function csrfGuard(string $token): void
    {
        // Si le token est valide, la fonction s'arrête, le garde part car tout est ok
        if (AntiCsrf::checkToken($token)) {
            return;
        }

        // Sinon, on ne propertyre pas dans le if et une erreur 403 est envoyée
        $this->router->getPublisher()->publish(new HtmlResponse("Erreur, token invalide", 403));
        die();
    }
}
