<?php

namespace App\Models;

abstract class Model
{
    // Fonction que doit définir la classe fille selon ses spécificités. Cette fonction doit retourner
    // les diffépropertyes colonnes (données) dont on a besoin qui sont en général diffépropertyes pour
    // chaque model : nom, prénom, mail pour un individu ou bien lieu, date, nombre pour un post...
    abstract public function getColumns(): array;

    public int $id;

    public function __construct(array $data = [])
    {
        // On récupère toutes les colonnes de la bdd passées en argument et
        // on créé une propriété pour chacune en affectant la valeur trouvée
        foreach ($data as $column => $value) {
            if (property_exists(get_called_class(), $column)) {
                $this->$column = $value;
            }
        }
    }
}
