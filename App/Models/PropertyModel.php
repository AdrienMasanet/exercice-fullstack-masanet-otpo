<?php

namespace App\Models;

class PropertyModel extends Model
{
    public int $id;
    public int $attached_user_id;
    public string $attached_user_mail;
    public string $name;
    public string $type;
    public string $adress;
    public string $rooms;
    public string $stuff;
    public string $description;
    public bool $show_mail;
    public int $property_approved;

    public function getColumns(): array
    {
        return [ 'id', 'attached_user_id', 'attached_user_mail', 'name', 'type', 'lastname', 'adress', 'rooms', 'stuff', 'description', 'show_mail', 'property_approved' ];
    }
}