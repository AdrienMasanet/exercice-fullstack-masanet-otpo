<?php

namespace App\Models;

class UserModel extends Model
{
    public string $email;
    public string $firstname;
    public string $lastname;
    public string $password;
    public bool $is_admin;
    public bool $is_pro;

    public function getColumns(): array
    {
        return [ 'email', 'firstname', 'lastname', 'password', 'is_admin', 'is_pro' ];
    }

    public function getFullName(): string
    {
        return sprintf( '%s %s', $this->firstname, $this->lastname );
    }
}