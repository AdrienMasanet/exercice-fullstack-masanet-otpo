<?php

namespace App\Views;

define("TEMPLATE_PATH", APP_PATH . "Views" . DS . "templates" . DS);

class View
{
    private string $templateName;
    // la variable templateOnly permet de pouvoir afficher une view sans la topbar et le footer
    public bool $templateOnly = false;

    public function __construct(string $templateName)
    {
        $this->templateName = $templateName;
    }

    public function render(array $data = []): void
    {
        // On permet à la view d'utiliser la data qui lui est transmise par le controller
        // qui lui est propre
        extract($data);

        // On charge le header contenant le head et les balises meta
        require_once TEMPLATE_PATH . "partials" . DS . "_header.php";

        // Si la view n'est pas en templateOnly, on affiche la topbar
        if (!$this->templateOnly) {
            require_once TEMPLATE_PATH . "partials" . DS . "_topbar.php";
        }
        
        // On ouvre la div de main content qui contient l'id="main_content"
        require_once TEMPLATE_PATH . "partials" . DS . "_main_content_begin.php";

        // On charge la view demandée qui peut être la page index, la page connexion etc...
        require_once TEMPLATE_PATH . $this->templateName . ".php";
        
        // On ferme la div de main content qui contient l'id="main_content"
        require_once TEMPLATE_PATH . "partials" . DS . "_main_content_end.php";

        // Si la view n'est pas en templateOnly, on affiche le footer complet, sinon on ferme juste les balises
        if (!$this->templateOnly) {
            require_once TEMPLATE_PATH . "partials" . DS . "_footer.php";
        } else {
            require_once TEMPLATE_PATH . "partials" . DS . "_footer_minimal.php";
        }
    }
}
