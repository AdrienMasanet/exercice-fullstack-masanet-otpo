<h1 class="title"><span class="red">O</span>ffice du <span class="red">T</span>ourisme des <span class="red">P</span>yrénées <span class="red">O</span>rientales</h1>
<h3 class="center">Accueil</h3>

<br>

<div id="map" style="width:1600px; height: 600px" class="center"></div>

<!-- CHARGEMENT CSS DE MAPBOX -->
<link href="https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.css" rel='stylesheet' />
<!-- CHARGEMENT JS DE MAPBOX -->
<script src="https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.js"></script>
<script src="https://unpkg.com/es6-promise@4.2.4/dist/es6-promise.auto.min.js"></script>
<script src="https://unpkg.com/@mapbox/mapbox-sdk/umd/mapbox-sdk.min.js"></script>
<script src="<?php echo (JS_PATH . "mapbox.js"); ?>"></script>