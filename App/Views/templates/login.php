<h1 class="title">Connexion</h1>

<?php if (!is_null($form_status)) : ?>
    <div class="center mb"><?php echo $form_status->message ?></div>
<?php endif; ?>

<div class="fitContainer">
    <form class="center" action="/login" method="post">
        <input type="hidden" name="csrf" value="<?php echo $csrf_token; ?>">

        <label>
            <span>Email : </span><br>
            <input type="email" name="email" required>
        </label>
        <br>
        <br>

        <label>
            <span>Mot de passe : </span><br>
            <input type="password" name="password" required>
        </label>
        <br>
        <br>
        <input class="button" type="submit" value="Connexion">
    </form>

    <a class="button" href="/">Retour à l'accueil</a>
</div>