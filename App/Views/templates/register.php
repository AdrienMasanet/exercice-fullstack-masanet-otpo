<h1 class="title">Créer un compte</h1>

<?php if (!is_null($form_status)) : ?>
    <div class="center mb"><?php echo $form_status->message ?></div>
<?php endif; ?>

<div class="fitContainer">
    <form class="center" action="/register" method="post">
        <input type="hidden" name="csrf" value="<?php echo $csrf_token; ?>">

        <label>
            <span>Prénom : </span><br>
            <input type="text" name="firstname" required>
        </label>
        <br>
        <br>

        <label>
            <span>Nom : </span><br>
            <input type="text" name="lastname" required>
        </label>
        <br>
        <br>

        <label>
            <span>Email : </span><br>
            <input type="email" name="email" required>
        </label>
        <br>
        <br>

        <label>
            <span>Mot de passe : </span><br>
            <input type="password" name="password" required>
        </label>
        <br>
        <br>

        <label>
            <span>Vous êtes un :</span><br>
            <div>
                <input type="radio" id="checkbox_pro_no" name="checkbox_pro" value="0" checked>
                <label for="checkbox_pro_no">Touriste</label>
            </div>

            <div>
                <input type="radio" id="checkbox_pro_yes" name="checkbox_pro" value="1">
                <label for="checkbox_pro_yes">Professionnel</label>
            </div>
        </label>
        <br>
        <br>
        <input class="button" type="submit" value="Valider">
    </form>

    <a class="button" href="/">Retour à l'accueil</a>
</div>