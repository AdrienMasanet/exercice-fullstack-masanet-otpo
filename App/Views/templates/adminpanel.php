<h1 class="title">Panneau d'administration</h1>
<h3 class="center">Choisissez l'action souhaitée</h3>
<br>
<div class="fitContainer">
    <ul class="flex np linodot adminCommands">
        <li>
            <a class="button center" href="/admin/manageusers">Gérer les utilisateurs inscrits sur le site</a>
        </li>
        <li>
            <a class="button center" href="/admin/addpro">Gérer les hébergeurs validés et non validés</a>
        </li>
        <li>
            <a class="button center" href="/admin/validateproperty">Gérer les demandes d'ajout de biens</a>
        </li>
    </ul>
</div>