<h1 class="title">Panneau d'administration</h1>
<H3 class="center mb2">Gérer tous les utilisateurs inscrits sur le site</h3>

<div class="nfw">
    <?php
    foreach ($users as $user) {
        echo "<div class='userListContainer fw mb' id='container_user_" . $user->id . "'>";
    ?>
        <div class="userListTitle">
            <div><?php echo "Utilisateur n°" . $user->id ?></div>
        </div>
        <div class="userListDetailsContainer flex flexCenter">
            <tr>
                <div>
                    <p> Nom :<br> <strong> <?php echo $user->lastname ?> </strong> </p>
                </div>
                <div>
                    <p> Prénom :<br> <strong> <?php echo $user->firstname ?> </strong> </p>
                </div>
                <div>
                    <p> Adresse mail :<br> <strong> <?php echo $user->email ?> </strong> </p>
                </div>
                <div>
                    <p> Pro :
                        <br>
                        <strong>
                            <?php
                            if ($user->is_pro) { ?>
                                <span class='slightTextShadow big green'>✓</span>
                            <?php } else { ?>
                                <span class='slightTextShadow big red'>✗</span>
                            <?php } ?>
                        </strong>
                    </p>
                </div>
                <div>
                    <form class="button" action="/admin/deleteuser<?php echo $user->id; echo "/"; echo "manageusers"; ?>" method="post">
                        <input type="hidden" name="csrf" value="<?php echo $csrf_token; ?>">
                        <input type="hidden" name="id" value="<?php echo $user->id; ?>">
                        <input type="hidden" name="redirection" value="<?php echo "manageusers"; ?>">
                        <input class="fw fh" type="submit" value='Supprimer'>
                    </form>
                </div>
            </tr>
        </div>
    <?php
        echo "</div>";
    }
    ?>

    <a class="button center" href="/admin">Revenir au menu de l'interface d'administration</a>
</div>