<h1 class="title">Panneau d'administration</h1>
<h3 class="center mb2">Accepter les demandes d'ajout de biens</h3>

<div class="nfw">
    <?php
    if ($properties) {
    foreach ($properties as $property) {
        echo "<div class='propertyListContainer fw mb' id='container_property_" . $property->id . "'>";
    ?>
        <div class="propertyListTitle">
            <div><?php echo "Publication n°" . $property->id ?></div>
        </div>
        <div class="propertyListDetailsContainer flex flexCenter">
            <tr>
                <div>
                    <p> Intitulé :<br> <strong> <?php echo $property->name ?> </strong> </p>
                </div>

                <div>
                    <p> Type :<br> <strong> <?php echo $property->type ?> </strong> </p>
                </div>

                <div>
                    <p> Adresse :<br> <strong> <?php echo $property->adress ?> </strong> </p>
                </div>

                <div>
                    <p> Équipement :<br> <strong> <?php echo $property->stuff ?> </strong> </p>
                </div>

                <div>
                    <p> Description :<br> <strong> <?php echo $property->description ?> </strong> </p>
                </div>

                <div>
                    <form class="button" action="/admin/validateproperty/validate<?php echo $property->id; ?>" method="post">
                        <input type="hidden" name="csrf" value="<?php echo $csrf_token; ?>">
                        <input type="hidden" name="id" value="<?php echo $property->id; ?>">
                        <input class="fw fh" type="submit" value='Légitimer'>
                    </form>
                    <form class="button" action="/admin/validateproperty/cancel<?php echo $property->id; ?>" method="post">
                        <input type="hidden" name="csrf" value="<?php echo $csrf_token; ?>">
                        <input type="hidden" name="id" value="<?php echo $property->id; ?>">
                        <input class="fw fh" type="submit" value='Supprimer'>
                    </form>
                </div>
            </tr>
        </div>
    <?php
        echo "</div>";
    }
}
    ?>

    <a class="button center" href="/admin">Revenir au menu de l'interface d'administration</a>
</div>