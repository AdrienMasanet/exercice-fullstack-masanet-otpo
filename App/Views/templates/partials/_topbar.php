<header>
    <nav id="topBar">
        <?php
        // On vérifie si l'utilisateur est authentifié
        use App\Session;

        $user = Session::get(Session::SESSION_USER);
        $isAuth = !is_null($user);
        $is_admin = $isAuth && $user->is_admin;
        $is_pro = $isAuth && $user->is_pro;
        ?>

        <a href="/" class="logo">
            <img src="<?php echo (IMG_PATH . "logo.png"); ?>" />
        </a>

        <ul class="inlineFlex flexCenter fw menu">

            <li class="topbarMenuItem">
                <a href="/">Accueil</a>
            </li>

            <li class="topbarMenuItem">
                <a href="/">Menu 2</a>
            </li>

            <li class="topbarMenuItem">
                <a href="/">Menu 3</a>
            </li>

            <li class="topbarMenuItem">
                <a href="/">Menu 4</a>
            </li>

            <li class="topbarMenuItem">
                <a href="/">Menu 5</a>
            </li>

            <?php
            // Si l'utilisateur est un admin, on ajoute le lien vers l'interface d'administration
            if ($is_admin) {
                echo "<li class='topbarMenuItem'>";
                echo "<a href='/admin'>Interface d'administration</a>";
                echo "</li>";
            }

            // Si l'utilisateur est un admin ou un pro, on ajoute le lien vers l'interface permettant d'ajouter un bien à louer au site
            if ($is_pro || $is_admin) {
                echo "<li class='topbarMenuItem'>";
                echo "<a href='/addproperty'>Ajouter un bien</a>";
                echo "</li>";
            }
            ?>
        </ul>

        <?php
        echo "<div class='connectionButton'>";

        // On créé un bouton de déconnexion si l'utilisateur est connecté et un
        // bouton de connexion et de création de compte si il ne l'est pas
        if ($isAuth) {
            if ($is_admin) {
                echo "<p>Bonjour, admin " . $user->firstname . " !<p>";
            } else {
                echo "<p>Bonjour " . $user->firstname . " !<p>";
            }
            echo  "<a class='button' href='/logout'>Déconnexion</a>";
        } else {
            echo "<p>Pas connecté<p>";
            echo "<a class='button' href='/login'>Connexion</a>";
            echo "<br>";
            echo "<a class='button' href='/register'>Créer un compte</a>";
        }
        echo  "</div>";
        ?>

    </nav>
</header>