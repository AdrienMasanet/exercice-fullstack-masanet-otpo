<h1 class="title">Panneau d'administration</h1>
<h3 class="center">Ajouter un bien au site</h3>

<br>

<div class="fitContainer">
    <form class="center" action="/addproperty" method="post">
        <input type="hidden" name="csrf" value="<?php echo $csrf_token; ?>">

        <label>
            <span>Nom du bien : </span><br>
            <input type="text" name="name" required>
        </label>
        <br>
        <br>

        <label>
            <span>Type de bien : </span><br>
            <div>
                <input type="radio" id="checkbox_property_furniture" name="checkbox_property_type" value="Appartement meublé" checked>
                <label for="checkbox_property_furniture">Appartement meublé</label>
            </div>

            <div>
                <input type="radio" id="checkbox_property_lodging" name="checkbox_property_type" value="Gîte">
                <label for="checkbox_property_lodging">Gîte</label>
            </div>

            <div>
                <input type="radio" id="checkbox_house_for_rent" name="checkbox_property_type" value="Maison à la location">
                <label for="checkbox_house_for_rent">Maison à la location</label>
            </div>

            <div>
                <input type="radio" id="checkbox_private_mobile_home" name="checkbox_property_type" value="Mobile home privatif (hors camping)">
                <label for="checkbox_private_mobile_home">Mobile home privatif (hors camping)</label>
            </div>
        </label>
        <br>

        <label>
            <span>Adresse du bien : </span><br>
            <textarea rows="5" cols="50" name="adress" placeholder="Veuillez renseigner l'adresse du bien" required></textarea>
        </label>
        <br>
        <br>

        <label>
            <span>Nombre de pièces : </span><br>
            <input type="number" name="rooms" value="1" required>
        </label>
        <br>
        <br>

        <label>
            <span>Équipement : </span><br>
            <textarea rows="10" cols="50" name="stuff" placeholder="Veuillez renseigner l'équipement" required></textarea>
        </label>
        <br>
        <br>

        <label>
            <span>Description : </span><br>
            <textarea rows="10" cols="50" name="description" placeholder="Veuillez renseigner une description" required></textarea>
        </label>
        <br>
        <br>

        <label>
            <span>Afficher votre adresse mail sur l'annonce :</span><br>
            <div>
                <input type="radio" id="checkbox_show_mail_yes" name="checkbox_show_mail" value="1" checked>
                <label for="checkbox_show_mail_yes">Oui</label>
            </div>

            <div>
                <input type="radio" id="checkbox_show_mail_no" name="checkbox_show_mail" value="0">
                <label for="checkbox_show_mail_no">Non</label>
            </div>
        </label>
        <br>
        <input class="button" type="submit" value="Valider">
    </form>

    <a class="button center" href="/">Revenir à l'accueil</a>
</div>