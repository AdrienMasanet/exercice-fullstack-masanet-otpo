<?php

namespace App;

class Utils
{
    // Fonction permettant de hasher rapidement une string en sha512
    public static function passwordHash( string $password ): string
    {
        return hash( 'sha512', SALT . $password . PEPPER );
    }
}