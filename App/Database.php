<?php

namespace App;

use PDO;
use PDOException;

// Singleton de la classe Database qui gère la connexion
class Database
{
    private static ?self $instance = null;

    private ?PDO $pdo = null;

    public static function getInstance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getPdo(): ?PDO
    {
        if (is_null($this->pdo)) {
            // variable stockant l'adresse de la base de données
            $dsn = "mysql:dbname=" . DB_NAME . ";host=" . DB_HOST;

            // Options de PDO
            $pdo_options = [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ];

            // On tente de se connecter à la base de données, on retourne null si ça échoue
            try {
                $this->pdo = new PDO($dsn, DB_USER, DB_PASS, $pdo_options);
            } catch (PDOException $pdo_e) {
                return null;
            }
        }
        return $this->pdo;
    }
    
    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}
}
