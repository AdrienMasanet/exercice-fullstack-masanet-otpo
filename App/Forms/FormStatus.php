<?php

namespace App\Forms;

class FormStatus
{
    public bool $success;
    public array $errors = [];
    public string $message;
}
