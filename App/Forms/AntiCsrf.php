<?php

namespace App\Forms;

use DateTime;

use App\Session;

class AntiCsrf
{
    // Fonction qui génère le token et qui l'affecte à la session actuelle
    public static function generateToken(string $additionalData = ""): string
    {
        // On récupère l'horodatage au moment de créer le token et on le concatène aux autres
        // variables que l'on a choisi pour génerer le token en sha512
        $timestamp = (new DateTime())->getTimestamp();
        $token = hash("sha512", SALT . session_id() . $timestamp . $additionalData . PEPPER);

        Session::set(Session::SESSION_ANTI_CSRF_TOKEN, $token);
        
        return $token;
    }

    //
    public static function checkToken(string $token): bool
    {
        return $token === Session::get(Session::SESSION_ANTI_CSRF_TOKEN);
    }
}
