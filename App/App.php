<?php

namespace App;

// Import de la librairie permettant le routage facile
use MiladRahimi\PhpRouter\Router;

// Import des controllers
use App\Controllers\PageController;
use App\Controllers\AuthController;
use App\Controllers\RegController;
use App\Controllers\AdminController;
use App\Controllers\ProController;

// Singleton de la classe App qui gère tout
class App
{
    private static ?self $instance = null;

    private ?Router $router = null;

    public static function getInstance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function start(): void
    {
        // On initialise la session pour qu'elle persiste
        session_start();
        $app = self::getInstance();
        $app->initRouter();
    }

    public function getRouter(): ?Router
    {
        return $this->router;
    }

    private function initRouter(): void
    {
        $this->router = Router::create();
        $this->initRoutes();
        $this->router->dispatch();
    }

    private function initRoutes(): void
    {
        // Pages standard liées au PageController
        $this->router->get('/', [PageController::class, 'index']);
        $this->router->get('/getmapmarkers', [PageController::class, 'returnMapMarkers']);

        // Pages liées à l'authentification donc au AuthController
        $this->router->get('/login', [AuthController::class, 'loginView']);
        $this->router->post('/login', [AuthController::class, 'login']);
        $this->router->get('/logout', [AuthController::class, 'logout']);

        // Pages liées à l'enregistrement d'un nouveau compte donc au RegController
        $this->router->get('/register', [RegController::class, 'registerView']);
        $this->router->post('/register', [RegController::class, 'register']);

        // Ici, toutes les routes réservées seulement aux utilisateurs admins
        $this->router->group(['adminFilter' => [AdminMiddleware::class]], function (Router $router) {
            // Panneau d'administration de base
            $router->get('/admin', [AdminController::class, 'admin']);

            // Panneau d'administration pour gérer tous les utilisateurs inscrits sur le site
            $router->get('/admin/manageusers', [AdminController::class, 'manageUsers']);

            // Panneau d'administration pour valider les utilisateurs qui veulent devenir pro
            $router->get('/admin/addpro', [AdminController::class, 'addPro']);
            $router->post('/admin/addpro/togglepro{id}', [AdminController::class, 'togglePro']);
            $router->post('/admin/deleteuser{id}/{redirection}', [AdminController::class, 'deleteUser']);

            // Panneau d'administration pour valider les demandes de publication des propriétés sur la map du site
            $router->get('/admin/validateproperty', [AdminController::class, 'validatePropertiesView']);
            $router->post('/admin/validateproperty/validate{id}', [AdminController::class, 'validateProperty']);
            $router->post('/admin/validateproperty/cancel{id}', [AdminController::class, 'removeProperty']);

            // Panneau commun aux admins et aux utilisateurs pro pour rajouter une propriété
            // (les propriétés rajoutées par les admins sont automatiquement validées)
            $router->get('/addproperty', [ProController::class, 'addPropertyView']);
            $router->post('/addproperty', [ProController::class, 'addProperty']);
        });

        // Ici, toutes les routes réservées seulement aux utilisateurs hébergeurs de bien
        $this->router->group(['proFilter' => [ProMiddleware::class]], function (Router $router) {
            // Panneau commun aux admins et aux utilisateurs pro pour rajouter une propriété
            $router->get('/addproperty', [ProController::class, 'addPropertyView']);
            $router->post('/addproperty', [ProController::class, 'addProperty']);
        });
    }

    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}
}
