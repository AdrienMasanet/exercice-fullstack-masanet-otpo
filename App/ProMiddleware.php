<?php

namespace App;

use Closure;
use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;

class ProMiddleware
{
    // Fonction qui permet de filtrer les utilisateurs qui ne sont pas des admins, afin de réserver
    // toutes les fonctions qui suivent aux utilisateurs connectés en tant qu'admins
    public function handle(ServerRequestInterface $request, Closure $next)
    {
        $user = Session::get(Session::SESSION_USER);
        $isAuth = !is_null($user);
        $is_pro = $isAuth && $user->is_pro;

        if ($is_pro) {
            return $next($request);
        }

        return new HtmlResponse('Vous n\'avez pas l\'autorisation d\'accéder à cette page', 403);
    }
}
