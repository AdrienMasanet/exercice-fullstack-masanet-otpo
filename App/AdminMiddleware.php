<?php

namespace App;

use Closure;
use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ServerRequestInterface;

class AdminMiddleware
{
    // Fonction qui permet de filtrer les utilisateurs qui ne sont pas des admins, afin de réserver
    // toutes les fonctions qui suivent aux utilisateurs connectés en tant qu'admins
    public function handle(ServerRequestInterface $request, Closure $next)
    {
        $user = Session::get(Session::SESSION_USER);
        $isAuth = !is_null($user);
        $is_admin = $isAuth && $user->is_admin;

        if ($is_admin) {
            return $next($request);
        }

        return new HtmlResponse('Vous n\'avez pas l\'autorisation d\'accéder à cette page', 403);
    }
}
