// On initialise la mapbox avec notre token et en définissant quelques configs basiques
mapboxgl.accessToken = 'pk.eyJ1IjoiYWRyaWVubWFzYW5ldCIsImEiOiJja24wMmZiMDgwOGlyMnVyeW5nOThnNjY1In0.NZsNdh1rS8PPIwgk3qgdyg';
var mapboxClient = mapboxSdk( {
    accessToken: mapboxgl.accessToken
} );

var map = new mapboxgl.Map( {
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [ 2.169562, 42.6259409 ],
    zoom: 9
} );

// On get en AJAX la route qui retourne le fichier json contenant marqueurs
$.get( "/getmapmarkers", function ( data )
{
    data = JSON.parse( data );

    // On lit chaque marqueur contenu dans le json pour en récupérer les données et le créér sur la map
    data.forEach( function ( marker )
    {

        var markerDiv = document.createElement( 'div' );

        switch ( marker.type )
        {
            case "Gîte":
                markerDiv.className = 'markerLodging';
                break;
            case "Appartement meublé":
                markerDiv.className = 'markerAppartment';
                break;
            case "Maison à la location":
                markerDiv.className = 'markerhouse';
                break;
            case "Mobile home privatif (hors camping)":
                markerDiv.className = 'markerMobilehome';
                break;
        }

        // On transforme l'adresse du marqueur pour la convertir en longitude/latitude
        mapboxClient.geocoding.forwardGeocode( {
            query: marker.adress,
            autocomplete: false,
            limit: 1
        } )
            .send()
            .then( function ( response )
            {
                if ( response && response.body && response.body.features && response.body.features.length )
                {
                    // On créé l'objet contenant les propriétés dynamiques du marker connecté à l'élément HTML markerDiv
                    var markerAttributes = new mapboxgl.Marker( markerDiv )

                    // On stocke l'adresse traduite en longitude/latitude par geocode dans la variable feature et on l'affecte
                    var feature = response.body.features[ 0 ];
                    markerAttributes.setLngLat( feature.center )

                    // Pour rajouter une fiche au marqueur lorsqu'on clique dessus
                    var markerAttributesPopup = new mapboxgl.Popup( {
                        offset: 25
                    } );

                    // Contenu de cette fiche
                    if ( marker.show_mail )
                    {
                        markerAttributesPopup.setHTML( '<h3>' + marker.name + '</h3><p><strong>DESCRIPTION :</strong></p><p>' + marker.description + '</p><p><strong>ÉQUIPEMENT :</strong></p><p>' + marker.stuff + '</p><p class="mapbox-description-mail">' + marker.attached_user_mail + '</p>' );
                    } else
                    {
                        markerAttributesPopup.setHTML( '<h3>' + marker.name + '</h3><p><strong>DESCRIPTION :</strong></p><p>' + marker.description + '</p><p><strong>ÉQUIPEMENT :</strong></p><p>' + marker.stuff + '</p>' );
                    }

                    markerAttributes.setPopup( markerAttributesPopup );
                    // On rajoute le marker à la map
                    markerAttributes.addTo( map );
                }
            } );
    } );
} );