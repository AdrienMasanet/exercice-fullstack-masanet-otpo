<?php

namespace App\Repositories;

use App\Models\UserModel;
use App\Utils;
use App\Session;
use App\Forms\FormStatus;

class UserRepository extends Repository
{
    public function getTable(): string
    {
        return "users";
    }

    public function login(string $email, string $password): ?UserModel
    {
        $query = "SELECT * FROM `users` WHERE `email`=:email AND `password`=:password";
        $statement = $this->pdo->prepare($query);

        // On cherche dans la bdd si une row correspondant au mail et au mdp hashé est trouvée
        $statement->execute([
            "email" => $email,
            "password" => Utils::passwordHash($password)
        ]);

        // Si une row avec ce mail et mdp est trouvée, on créé un nouvel objet user et on lui affecte
        // automatiquement comme propriétés toutes les variables des colonnes de cette row
        if ($statement && $statement->rowCount() > 0) {
            $user = new UserModel($statement->fetch());
            $user->password = "";

            return $user;
        }
        return null;
    }

    // Fonction enregistrant un nouvel utilisateur avec les données passées en argument, seulement
    // si le mail n'existe pas déjà dans la base de données
    public function register(string $firstName, string $lastName, string $email, string $password, string $checkbox_pro): bool
    {
        // On fait en sorte que l'utilisateur ne puisse pas s'enregistrer si la même adresse mail est
        // trouvée dans la base de données avant de créer la nouvelle row
        $query = "SELECT * FROM `users` WHERE `email`=:email";
        $statement = $this->pdo->prepare($query);

        // On cherche dans la bdd si une row correspondant au mail est trouvée
        $statement->execute([
            "email" => $email
        ]);

        // Si il existe au moins une colonne avec ce mail
        if ($statement && $statement->rowCount() > 0) {

            // On créé un nouveau statut de formulaire avec un avertissement en message
            $status = new FormStatus();
            $status->success = false;
            $status->message = "Une adresse mail pour ce compte existe déjà";
            Session::set(Session::SESSION_FORM_STATUS, $status);

            // On termine la fonction
            return false;
        }

        $query = "INSERT INTO `users` (`firstname`, `lastname`, `email`, `password`, `pro_approval_request`) VALUES ( :firstname, :lastname, :email, :password, :pro_approval_request)";
        $statement = $this->pdo->prepare($query);

        $statement->execute([
            "firstname" => $firstName,
            "lastname" => $lastName,
            "email" => $email,
            "password" => Utils::passwordHash($password),
            "pro_approval_request" => $checkbox_pro
        ]);

        return true;
        // Pour connecter automatiquement l'utilisateur après qu'il se soit enregistré, on pourrait mettre un $this->login($email,$password);
    }

    public function findAllUsers(): array
    {
        return $this->findAll(UserModel::class);
    }

    public function findUserById(int $id): ?UserModel
    {
        return $this->findById(UserModel::class, $id);
    }

    public function findAllUsersWaitingForApproval(): array
    {
        return $this->findAllByColumnValue(UserModel::class, "pro_approval_request", 1);
    }

    public function findAllProUsers(): array
    {
        return $this->findAllByColumnValue(UserModel::class, "is_pro", 1);
    }

    public function deleteById($id): void
    {
        $query = "DELETE FROM `users` WHERE `id`=:id";
        $statement = $this->pdo->prepare($query);

        $statement->execute([
            "id" => $id,
        ]);
    }

    public function toggleIsPro($id): void
    {
        $user = $this->findUserById($id);

        if (!$user->is_pro) {
            $query = "UPDATE `users` SET is_pro = '1', pro_approval_request= '0' WHERE `id`=:id";
        } else {
            $query = "UPDATE `users` SET is_pro = '0', pro_approval_request= '1' WHERE `id`=:id";
        }

        $statement = $this->pdo->prepare($query);

        $statement->execute([
            "id" => $id,
        ]);
    }

    public function __construct()
    {
        parent::__construct();

        $query = "CREATE TABLE IF NOT EXISTS `users` (
            `id` int(50) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
            `firstname` varchar(50) DEFAULT NULL,
            `lastname` varchar(50) DEFAULT NULL,
            `email` varchar(100) DEFAULT NULL,
            `password` varchar(128) DEFAULT NULL,
            `is_admin` tinyint(1) DEFAULT '0',
            `is_pro` tinyint(1) DEFAULT '0',
            `pro_approval_request` int(11) NOT NULL DEFAULT '0'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $statement = $this->pdo->prepare($query);
        $statement->execute();
    }
}
