<?php

namespace App\Repositories;

use PDO;
use Laminas\Diactoros\Response\HtmlResponse;

use App\App;
use App\Database;
use App\Models\Model;

abstract class Repository
{
    // C'est la classe qui hérite de celle-ci qui doit implémenter la function getTable()
    abstract public function getTable(): string;

    public ?PDO $pdo = null;

    public function __construct()
    {
        $this->pdo = Database::getInstance()->getPdo();

        if (is_null($this->pdo)) {
            App::getInstance()->getRouter()->getPublisher()->publish(new HtmlResponse("Erreur de la base de données sur le serveur", 500));
            die();
        }
    }

    // Retourne toutes les lignes dans un tableau d'objets de type correspondant au nom de la table
    protected function findAll($objectName): array
    {
        $result = [];

        $query = sprintf("SELECT * FROM `%s`", $this->getTable());
        $statement = $this->pdo->query($query);

        if ($statement) {
            foreach ($statement as $row) {
                $model = new $objectName($row);
                $result[] = $model;
            }
        }
        return $result;
    }

    // Retourne un objet du type rentré en premier argument dont l'ID correspond au deuxième argument
    protected function findById(string $objectName, int $id): ?Model
    {
        $query = sprintf("SELECT * FROM `%s` WHERE id=:id", $this->getTable());
        $statement = $this->pdo->prepare($query);
        $statement->execute(["id" => $id]);

        if ($statement) {
            foreach ($statement as $row) {
                $model = new $objectName($row);
                return $model;
            }
        }
        return null;
    }

    // Cherche et retourne une liste d'objet pour toutes les colonnes
    // où le nom de colonne $columnName et la valeur $columnValue correspondent
    public function findAllByColumnValue(string $objectName, string $columnName, $columnValue): array
    {
        $result = [];

        $query = "SELECT * FROM " . $this->getTable() . " WHERE " . $columnName . " = :columnValue";
        $statement = $this->pdo->prepare($query);

        $statement->execute([
            "columnValue" => $columnValue
        ]);

        if ($statement) {
            foreach ($statement as $row) {
                $model = new $objectName($row);
                $result[] = $model;
            }
        }

        return $result;
    }

    // Pour la row correspondant à l'id $id, modifier la column choisie en argument
    // en y mettant la valeur rentrée en dernier argument
    public function changeColumnValueForId($id, $columnName, $columnValue): void
    {
        $query = "UPDATE " . $this->getTable() . " SET " . $columnName . " = :columnValue WHERE `id`=:id ";
        $statement = $this->pdo->prepare($query);

        $statement->execute([
            "columnValue" => $columnValue,
            "id" => $id
        ]);
    }
}
