<?php

namespace App\Repositories;

use App\Models\PropertyModel;
use App\RepositoryManager;

class PropertyRepository extends Repository
{
    public function getTable(): string
    {
        return "properties";
    }

    // Fonction enregistrant un nouvel utilisateur avec les données passées en argument, seulement
    // si le mail n'existe pas déjà dans la base de données
    public function addProperty(int $attached_user_id, string $attached_user_mail, string $name, string $type, string $adress, int $rooms, string $stuff, string $description, int $show_mail, int $property_approved)
    {

        $query = "INSERT INTO `properties` (`attached_user_id`, `attached_user_mail`, `name`, `type`, `adress`, `rooms`, `stuff`, `description`, `show_mail`, `property_approved`) VALUES ( :attached_user_id, :attached_user_mail, :name, :type, :adress, :rooms, :stuff, :description, :show_mail, :property_approved)";
        $statement = $this->pdo->prepare($query);

        $statement->execute([
            "attached_user_id" => $attached_user_id,
            "attached_user_mail" => $attached_user_mail,
            "name" => $name,
            "type" => $type,
            "adress" => $adress,
            "rooms" => $rooms,
            "stuff" => $stuff,
            "description" => $description,
            "show_mail" => $show_mail,
            "property_approved" => $property_approved
        ]);
    }

    public function findPropertyById(int $id): ?PropertyModel
    {
        return $this->findById(PropertyModel::class, $id);
    }

    public function findAllValidatedProperties(): array
    {
        return $this->findAllByColumnValue(PropertyModel::class, "property_approved", 1);
    }

    public function findAllPropertiesWaitingForApproval(): array
    {
        $properties = $this->findAllByColumnValue(PropertyModel::class, "property_approved", 0);

        foreach ($properties as $key => $property) {
            // On stocke l'id de l'utilisateur qui a créé ce post
            $ownerId = $this->findPropertyById($property->id)->attached_user_id;
            // On stocke cet utilisateur dans la variable $owner de type UserModel
            $owner = RepositoryManager::getRepositoryManager()->getUserRepository()->findUserById($ownerId);

            // Si le propriétaire de ce post n'est pas (ou n'est plus) pro
            if (!$owner->is_pro) {
                // On retire le bien actuel du tableau de biens à renvoyer au back office
                unset($properties[$key]);
            }
        }

        // On retourne l'array d'objets de type PropertyModel dans lequel il ne reste que des biens qui
        // viennent seulement d'utilisateurs actuellement reconnus comme étant pro validés
        return $properties;
    }

    public function deleteById($id): void
    {
        $query = "DELETE FROM `properties` WHERE `id`=:id";
        $statement = $this->pdo->prepare($query);

        $statement->execute([
            "id" => $id,
        ]);
    }

    public function changeApprovedStatus(int $id, int $status): void
    {
        $this->changeColumnValueForId($id, "property_approved", $status);
    }

    public function disapprovePropertiesIfOwnerIsNotPro(): void
    {
        $properties = $this->findAllValidatedProperties();

        foreach ($properties as $property) {
            // On stocke l'id de l'utilisateur qui a créé ce post
            $ownerId = $this->findPropertyById($property->id)->attached_user_id;
            // On stocke cet utilisateur dans la variable $owner de type UserModel
            $owner = RepositoryManager::getRepositoryManager()->getUserRepository()->findUserById($ownerId);

            // Si le propriétaire de ce post n'est pas (ou n'est plus) pro
            if (!$owner->is_pro) {
                // On remet ce post en non approuvé
                $this->changeApprovedStatus($property->id, 0);
            }
        }
    }

    public function __construct()
    {
        parent::__construct();

        $query = "CREATE TABLE IF NOT EXISTS `properties` (
            `id` int(50) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
            `attached_user_id` int(11) DEFAULT NULL,
            `attached_user_mail` varchar(50) DEFAULT NULL,
            `name` varchar(50) DEFAULT NULL,
            `type` text,
            `adress` text DEFAULT NULL,
            `rooms` int(11) DEFAULT NULL,
            `stuff` text,
            `description` text,
            `show_mail` int(11) DEFAULT NULL,
            `property_approved` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $statement = $this->pdo->prepare($query);
        $statement->execute();

        // Si l'utilisateur n'est pas ou n'est plus pro, on retire automatiquement l'approbation
        // de ce bien, ce qui l'enlève automatiquement de la map
        $properties = $this->findAllValidatedProperties();
    }
}
