<?php

// Macros définissant les authentifiants pour la base de donénes
define( 'DB_HOST', 'database' );
define( 'DB_NAME', 'lamp' );
define( 'DB_USER', 'lamp' );
define( 'DB_PASS', 'lamp' );

// Macros définissant le salt et le pepper pour la création des tokens
define( 'SALT', '498ZAEG7hg' );
define( 'PEPPER', '3F99g46977g' );

// Macros définissant le chemin des assets
define( 'ASSETS_PATH', ROOT_PATH . 'Assets' . DS );
define( 'CSS_PATH', ASSETS_PATH . 'css' . DS );
define( 'JS_PATH', ASSETS_PATH . 'js' . DS );
define( 'IMG_PATH', ASSETS_PATH . 'img' . DS );