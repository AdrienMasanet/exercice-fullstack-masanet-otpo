<?php

use App\App;

// Macros permettant d'alléger le code
define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT_PATH', dirname( __FILE__ ) . DS );
define( 'APP_PATH', ROOT_PATH . DS . 'App' . DS );

// On inclut le fichier de configuration contenant la déclaration de variables essentielles
require_once ROOT_PATH . 'config.php';

// Auto-chargement des classes propres au projet
spl_autoload_register();

// Auto-chargement des librairies installées par composer
require_once ROOT_PATH . 'vendor' . DS . 'autoload.php';

// On créé l'instance du singleton qui fait tourner l'application entière
App::start();