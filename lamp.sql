-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : database
-- Généré le : sam. 03 avr. 2021 à 15:54
-- Version du serveur :  5.7.29
-- Version de PHP : 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `lamp`
--

-- --------------------------------------------------------

--
-- Structure de la table `properties`
--

CREATE TABLE `properties` (
  `id` int(50) UNSIGNED NOT NULL,
  `attached_user_id` int(11) DEFAULT NULL,
  `attached_user_mail` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` text,
  `adress` text,
  `rooms` int(11) DEFAULT NULL,
  `stuff` text,
  `description` text,
  `show_mail` int(11) DEFAULT NULL,
  `property_approved` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `properties`
--

INSERT INTO `properties` (`id`, `attached_user_id`, `attached_user_mail`, `name`, `type`, `adress`, `rooms`, `stuff`, `description`, `show_mail`, `property_approved`) VALUES
(1, 7, 'hanniballecter@flesh.eat', 'Maison avec immense cuisine', 'Maison à la location', 'Canet en Roussillon', 6, 'Cuisine immense (pour la viande)\r\nCouteaux très tranchants (pour couper la viande)\r\nBonne ventilation (pour virer l\'odeur de la viande)\r\nCrochets pour étendre de la viande\r\nBureau en bois massif\r\nCanapé pour psychanalyser des patients', 'Maison parfaite pour cuisiner et faire des psychanalyses', 1, 1),
(2, 8, 'papaemeritus@ghost.faith', 'Gîte de pèlerinage', 'Gîte', 'Font romeu', 3, 'Croix retournées\r\nFond de teint blanc\r\nAlbums de ghost avec tourne-disque', 'ATTENTION : INTERDIT AUX EXORCISTES\r\nPetit gîte sympa où vous pourrez aduler la bête aux multiples noms en chantant', 1, 1),
(3, 9, 'billijoearmstrong@green.day', 'Appartement bien isolé', 'Appartement meublé', 'Bompas', 2, '2 4x12 Marshall\r\ndes stratocaster\r\npas mal de pédales d\'overdrive\r\ndes câbles jack qui trainent un peu partout\r\nMurs bien isolés', 'Venez faire vos répet ici, vous pouvez faire un max de bruit et au pire on s\'en fout des voisins', 0, 0),
(4, 12, 'djangoreinhardt@gipsy.caravan', 'Mobile home nomade', 'Mobile home privatif (hors camping)', 'Saint hippolyte', 1, 'Une guitare désacordée\r\nLe kit du petit allumeur de feu de camp\r\nUn fil de pêche pour étendre le linge dehors\r\nUne télé 8k\r\nUn évier en or massif', 'Petit mobile home tranquilou posé au beau milieu d\'une prairie, pour l\'électricité vous en faîtes pas j\'ai piraté la borne EDF de la commune à côté ça marche super', 1, 1),
(5, 21, 'jawadbendaoud@appart.wolah', 'Petit appart sympa', 'Appartement meublé', 'Millas', 2, 'Coussin de qualité supérieure pour dormir comme un roi\r\nGerbille super musclée dans sa cage\r\nFenêtre pour faire belek', 'Petit appart pour les gens peu fortunés', 0, 1),
(6, 21, 'jawadbendaoud@appart.wolah', 'Appart pas cher mais clairement paumé', 'Appartement meublé', 'Auzat', 4, 'Lunettes de soleil (c\'est la montagne faut faire gaffe)\r\nCouteau suisse pour se défendre en cas d\'attaque d\'ours sauvage\r\n2 chambres mais le chauffage est cassé', 'Venez vous détendre dans ce petit appart pas cher\r\nPar contre c\'est paumé je vous aurais averti', 0, 1),
(7, 22, 'sylvaindurif@lechrist.cosmique', 'Maison du christ cosmique', 'Maison à la location', 'Bugarach', 7, 'Vaisseaux lumières de la vierge marie\r\nPetite radio avec \"What\'s Going On\" de Four non blondes qui tourne en boucle', 'Venez louer la maison du grand Sylvain-Pierre Durif, alias Sylvanus, Sylboss, Merlin l\'Enchanteur, Oriana (nom d\'homme), le Christ-Cosmique, le Grand Monarque, l\'Homme Vert, le Messie, Al Khidr et Maitreya le lion blanc', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(50) UNSIGNED NOT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `is_pro` tinyint(1) DEFAULT '0',
  `pro_approval_request` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `is_admin`, `is_pro`, `pro_approval_request`) VALUES
(1, 'test', 'test', 'test@test', '03f632e2d08046d9bc08f608920598a57ee62b7e966f46fbc8d7f60975b905074871564605372da7e9a526cfd6ee27d8f1c63160f1371f4da3a317c272e488c6', 1, 0, 0),
(2, 'Gérard', 'Bidule', 'gerardbidule@yahoo.fr', '58e69809c4a84e6cb0ec1b43a0408e55ecb14bac6a5dbd68474ebf062a905b11ecb4351b767c22092d044e8bb26effb79cf465facb21d61be7bc78812d7ff4fd', 0, 0, 0),
(3, 'Eren', 'Jaeger', 'erenjaeger@snk.titan', 'cac6c74024fc99b3289efc7ca610d9acc519fa858ede8834b3b817f17b0ef32945c0e635102626f652c2abac3508b2ce8018aeafe1f89a6f071e9ed98b6a17cc', 0, 0, 0),
(4, 'Obi-wan', 'Kenobi', 'ObiwanKenobi@star.wars', 'f53980ad00c5d326b2a30d8b27eeb525fb21d9e44e4f924d80e5b7dddacf61351acf12c81a6605432979dacd6e492b4788d02f5643196098ff53d78d8923b3b0', 0, 0, 0),
(5, 'Qui-Gon', 'Jinn', 'quigonjinn@star.wars', '46381985318bab9d250e7551766dad9b4da9030fc40af199afc5e79fdbc8936c1e05502499c78680ed2f9a2a3b5e420b0229467a9d17bbe602dbb1150c3b4ced', 0, 0, 0),
(6, 'Jon', 'Snow', 'jon.snow@got.westeros', 'a5ff5dcbc0de0d08aeb7c0bae3d137c69c384788e9a9cca75176482111e0d03b8285d0e3a9e480a33c29180ce83376483138ea8f2a519fa38e9da36d768e06d1', 0, 0, 1),
(7, 'Hannibal', 'Lecter', 'hanniballecter@flesh.eat', '194015e7d8f26054c80a83be7f5455eff7532034bb38c8942b3f2cd5070b77e37772df2a681cb12232a32ffe488c60fdc941121f727768de19afa763238c0423', 0, 1, 0),
(8, 'Papa', 'Emeritus', 'papaemeritus@ghost.faith', 'cb2f8e972f0d12e19d4de3536a152eda40cc9656ff837a5185d15e67c9a448000410f17569f6a40c9b549860181a1c3fd7397604c1f5269e4fe8ed5a5c08e3f0', 0, 1, 0),
(9, 'Billie Joe', 'Armstrong', 'billijoearmstrong@green.day', '1e593bd5e8980bac492fc33e45915ac46f888c5008642b40ddccc4cbae7477efb727045c56d025ac4d3d6375aa1d52ad14295997c0d11a4e285e0c61ea8f7e4f', 0, 1, 0),
(10, 'Rick', 'Sanchez', 'ricksanchez@multi.dimensional', '075ad735f68d722c5ca6dc23a0639d8cf4284d749f4029b1fede2c04bb6d5abe140403cff115805df9e6fdc546a2eb5ea8d9d8d416de4a269d2937c3c2120dc8', 0, 0, 1),
(11, 'Marcus', 'Millers', 'marcusmillers@bass.slap', '3ce47a60a81b6e60f5cc313c5192e300ae156c601aabbcddc4380ce93bd5bc794362954819e67643f60a0913a65e648719350a94ec5fe5802d6aebb224976b24', 0, 1, 0),
(12, 'Django', 'Reinhardt', 'djangoreinhardt@gipsy.caravan', 'e5bb908777cf893e10015fa032ed9479fa3c982c7574201faaa02968713e5855151b42613f5ae22bb51e1165b863c7bee816106870246223ced1f4e490c3d398', 0, 1, 0),
(20, 'Rachid', 'Gentillhomme', 'rachidgentillhomme@cool.mec', 'a0ed7a7a1a42f36188863979673def2a4c29daf37f111f09d69b925887983c541bfc8110b6199dbe7bb37433467727decbd7d1b97b22d128b4457609291cc9ef', 0, 0, 1),
(21, 'Jawad', 'Bendaoud', 'jawadbendaoud@appart.wolah', '2b6a940d952a93513e48994988f710e8e6d4656b595c84c4b497cb2eb6f9686608c85f3950a72376ecae0402ff0bd541e4edc6e86c21d9f08d173b42ec61c0da', 0, 1, 0),
(22, 'Sylvain', 'Durif', 'sylvaindurif@lechrist.cosmique', 'a7eb15a4f159340358a055f637ffba3a853f19a4bf767c898d1841dadf3f481db84df1ff2acd5ce91273e2edba9ca1e96b4791e6f3cf711d0a23fea40948a125', 0, 1, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(50) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(50) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
